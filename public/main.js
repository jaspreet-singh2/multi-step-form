// Select various elements by their class names and IDs
let listofsetpnos = document.getElementsByClassName('step-no');
let listofstepContainers = document.getElementsByClassName('step-container');
let form = document.querySelector('.needs-validation');
let nextStep = document.querySelector('.NextStep');
let goBack = document.querySelector('.Goback');
let step1 = document.getElementById('step1');

// Select the monthly-to-yearly billing switch
let monthlytoyearly = document.getElementById('flexSwitchCheckChecked');
let Monthly = true;
let currentpage = 1;

// Function to collect and display data for step 4
function createstep4data() {
    // Get the currently checked plan
    let checked = document.querySelector('.btn-check:checked');
    let parent = checked.parentElement;
    let priceelemnt = parent.querySelector('.price');
    let planprice = priceelemnt.innerHTML;
    let planName = parent.querySelector('.planName').innerHTML;
    // Determine if the plan is monthly or yearly
    let monthlyORyearly = priceelemnt.getAttribute('data-year') == priceelemnt.innerHTML ? '(Yearly)' : '(Monthly)';
    // Display plan details
    document.querySelector('.planN').innerHTML = `${planName} ${monthlyORyearly}`;
    document.querySelector('.planP').innerHTML = `${planprice}`;
    let planpricenum = returnthenumber(planprice);

    // Hide unchecked add-ons
    let addOnsnotchecked = Array.from(document.querySelectorAll('.form-check-input.add-ons:not(:checked)'));
    addOnsnotchecked.forEach((checked) => {
        checkedtext = checked.parentElement.querySelector('h4').innerHTML;
        let identifier = checkedtext.split(" ")[1];
        document.querySelector(`.row.${identifier}`).classList.add('d-none');
    });

    // Show checked add-ons
    let addOnschecked = Array.from(document.querySelectorAll('.form-check-input.add-ons:checked'));
    addOnschecked.forEach((checked) => {
        checkedtext = checked.parentElement.querySelector('h4').innerHTML;
        let identifier = checkedtext.split(" ")[1];
        document.querySelector(`.row.${identifier}`).classList.remove('d-none');
    });

    // Calculate and display the total price
    calculatetotal(planpricenum);
    movetopage(++currentpage);
}

// Function to calculate the total price
function calculatetotal(planpricenum) {
    let total = planpricenum;
    Array.from(document.querySelectorAll('.final-addons')).forEach((ele) => {
        if (!ele.parentElement.parentElement.classList.contains('d-none')) {
            total += returnthenumber(ele.innerHTML);
        }
    });
    let monthlyORyearlytotal = Monthly ? 'mo' : 'yr';
    document.querySelector('.total-price').innerHTML = `$${total}/${monthlyORyearlytotal}`;
}

// Event listener for the monthly-to-yearly billing switch
monthlytoyearly.addEventListener('change', (event) => {
    if (event.currentTarget.checked) {
        Monthly = false;
        // Change plan prices to yearly
        Array.from(document.querySelectorAll('[data-year]')).forEach(
            (ele) => {
                ele.innerHTML = ele.getAttribute('data-year');
            }
        );
        // Show 2 months free for yearly plans
        Array.from(document.querySelectorAll('.mfree.d-none')).forEach(
            (ele) => {
                ele.classList.remove('d-none');
            }
        );
    } else {
        Monthly = true;
        // Change plan prices to monthly
        Array.from(document.querySelectorAll('[data-month]')).forEach(
            (ele) => {
                ele.innerHTML = ele.getAttribute('data-month');
            }
        );
        // Hide 2 months free for monthly plans
        Array.from(document.querySelectorAll('.mfree')).forEach(
            (ele) => {
                ele.classList.add('d-none');
            }
        );
    }
});

// Event listener for the "NextStep" button
nextStep.addEventListener('click', (event) => {
    if (currentpage === 1) {
        validation(form);
    } else if (currentpage === 3) {
        createstep4data();
        nextStep.innerHTML = 'Confirm';
    } else if (currentpage === 4) {
        // Hide "NextStep" and "GoBack" buttons on the last step
        nextStep.classList.add('d-none');
        goBack.classList.add('d-none');
        movetopage(++currentpage);
    } else {
        let nextpage = ++currentpage;
        movetopage(nextpage);
    }
});

// Event listener for the "GoBack" button
goBack.addEventListener('click', (event) => {
    let previousPage = --currentpage;
    if (previousPage === 1) {
        event.target.classList.add('d-none');
    }
    movetopage(previousPage);
});

// Function to move to a specific page in the form
function movetopage(pageNo) {
    let targetstep = document.querySelector(`[data-stepNo="${pageNo}"]`);
    let targetstepNo = document.querySelector(`[data-stepNum="${pageNo}"]`);
    let currentactivestepNo = document.querySelector('.active');
    let currentactivestep = document.querySelector('.step-container:not(.d-none)');
    currentactivestep.classList.add('d-none');
    targetstep.classList.remove('d-none');
    currentactivestepNo.classList.remove('active');
    targetstepNo.classList.add('active');
}

// Function for form validation
function validation(form) {
    let allinputs = Array.from(document.querySelectorAll('.form-control'));
    allinputs.forEach((input) => {
        if (input.validity.patternMismatch) {
            document.querySelector('.invalid-feedback.number').innerHTML = "Enter the correct pattern";
            form.classList.add('was-validated');
        }
        if (input.validity.typeMismatch) {
            document.querySelector('.invalid-feedback.email').innerHTML = "Enter the correct email pattern";
            form.classList.add('was-validated');
        }
        if (input.validity.valueMissing) {
            form.classList.add('was-validated');
        }
    });
    if (form.checkValidity()) {
        goBack.classList.remove('d-none');
        movetopage(2);
        currentpage = 2;
    }
}

// Function to extract the numeric part from a string
function returnthenumber(stringwithnumber) {
    var regex = /\d+/g;
    return Number(stringwithnumber.match(regex));
}
